#include<stdio.h>

int main(void)
{
	double num = 1234.56789;
	float num2 = 1234.56789;
	printf("%f\n", num);
	printf("%f", num2);
	return 0;
}

/* The last 2 digits are different. You use double when you are doing very precise calculations. 
If you don't need high precision numbers or calculations and you need to store a signed number, a float is enough. */
