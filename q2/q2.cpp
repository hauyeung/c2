#include<stdio.h>

int main(void)
{
	int num;
	printf("Enter an integer: ");
	scanf("%d", &num);
	printf("ASCII code %d corresponds to the character %c.\n", num, num);
	return 0;
}
